package entity;

import javax.persistence.Embeddable;

@Embeddable
public class Info {
	private String addr;
	private String tel;

	public Info(String addr, String tel) {
		super();
		this.addr = addr;
		this.tel = tel;
	}
	
	public Info(){
		
	}
	
	@Override
	public String toString() {
		return "info ["+", addr=" + addr + ", tel=" + tel + "]";
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
}
