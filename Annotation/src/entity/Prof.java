package entity;

import javax.persistence.*;

@Entity
public class Prof {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long P_id;
	private String nom;
	
	public Prof(String nom) {
		this.nom = nom;
	}
	
	public Prof(){
		
	}

	public Long getP_id() {
		return P_id;
	}

	public void setP_id(Long p_id) {
		P_id = p_id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Prof [P_id=" + P_id + ", nom=" + nom + "]";
	}
}
