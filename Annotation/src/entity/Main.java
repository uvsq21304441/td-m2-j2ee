package entity;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hibern.HibernateUtil;

public class Main {

	public static void main(String[] args){
		Course c1 = new Course("maths");
		Course c2 = new Course("informatique");
		Course c3 = new Course("geographie");
		
		createCourse(c1);
		createCourse(c2);
		createCourse(c3);
			
		Info i1 = new Info("addr1","n1");
		Info i2 = new Info("addr2","n2");
		Info i3 = new Info("addr3","n3");
		
		Prof p1 = new Prof("Quai7");
		Prof p2 = new Prof("Mautor");
		Prof p3 = new Prof("Vial");
		Prof p4 = new Prof("l'homme au chapeau");
		

		Set<Prof> s1 = new HashSet<Prof>(Arrays.asList(p1,p2));
		s1.forEach(e->System.out.println(e));
		
		Set<Prof> s2 = new HashSet<>();
		s2.add(p3);
		s2.add(p4);
		
		Set<Prof> s3 = new HashSet<Prof>();
		s3.add(p2);
		s3.add(p1);
		s3.add(p4);
		

		createProf(p1);
		createProf(p2);
		createProf(p3);
		createProf(p4);
		
		Etudiant e1 = new Etudiant("Yannick",i1,c1,s1);
		Etudiant e2 = new Etudiant("Dams",i2,c2,s2);
		Etudiant e3 = new Etudiant("Zaaaaaaaaaarour",i3,c3,s3);
		createEtudiant(e1);
		createEtudiant(e2);
		createEtudiant(e3);
	}
	
	public static void createProf(Prof p){
		Transaction tx = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{
			tx=session.beginTransaction();
			
			session.save(p);
			
			tx.commit();
		}catch(HibernateException e){
			System.out.println("coucou");
			tx.rollback();
		}
	}
	public static void createCourse(Course c){
		Transaction tx = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{
			tx=session.beginTransaction();
			session.save(c);
			
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}
	}
	public static void createEtudiant(Etudiant et){
		Transaction tx = null;
		Session session = HibernateUtil.getSessionFactory().getCurrentSession();
		try{
			tx=session.beginTransaction();
			
			session.save(et);
			
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}
	}
}
