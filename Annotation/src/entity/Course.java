package entity;

import javax.persistence.*;

@Entity
public class Course {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long C_id;
	private String ue;
	
	public Course(String ue) {
		super();
		this.ue = ue;
	}
	
	public Course(){
		
	}

	public long getC_id() {
		return C_id;
	}

	public void setC_id(long c_id) {
		C_id = c_id;
	}

	public String getUe() {
		return ue;
	}

	public void setUe(String ue) {
		this.ue = ue;
	}

	@Override
	public String toString() {
		return "Cursus [C_id=" + C_id + ", ue=" + ue + "]";
	}
}
