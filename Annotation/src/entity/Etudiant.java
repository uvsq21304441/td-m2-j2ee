package entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
public class Etudiant {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long etudiantId;
	@Column(name = "ETUDIANT_NAME",nullable=false,length=100)
	private String name;
	private Info infoPerso;
	@ManyToOne(cascade = CascadeType.ALL)
	private Course cours;
	@ManyToMany
	private Set<Prof> profs = new HashSet<Prof>(0);
	
	public Etudiant(String name, Info infoPerso, Course cours, Set<Prof> profs) {
		this.name = name;
		this.infoPerso = infoPerso;
		this.cours = cours;
		this.profs = profs;
	}
	
	public Etudiant(){
		
	}

	public long getEtudiantId() {
		return etudiantId;
	}

	public void setEtudiantId(long etudiantId) {
		this.etudiantId = etudiantId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Info getInfoPerso() {
		return infoPerso;
	}

	public void setInfoPerso(Info infoPerso) {
		this.infoPerso = infoPerso;
	}

	public Course getCours() {
		return cours;
	}

	public void setCours(Course cours) {
		this.cours = cours;
	}

	public Set<Prof> getProfs() {
		return profs;
	}

	public void setProfs(Set<Prof> profs) {
		this.profs = profs;
	}

	@Override
	public String toString() {
		return "Etudiant [etudiantId=" + etudiantId + ", name=" + name + ", infoPerso=" + infoPerso + ", cours=" + cours
				+ ", profs=" + profs + "]";
	}
	
}
