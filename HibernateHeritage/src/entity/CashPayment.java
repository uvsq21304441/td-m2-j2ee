package entity;

public class CashPayment extends Payment{

	private String monnaie;
	
	public CashPayment(){ //Pour hibernate
	}

	public CashPayment(String description, String monnaie) { //construire la mere
		super(description);
		this.monnaie = monnaie;
	}

	public String getMonnaie() {
		return monnaie;
	}

	public void setMonnaie(String monnaie) {
		this.monnaie = monnaie;
	}

	@Override
	public String toString() {
		return "CashPayment [monnaie=" + monnaie + "]";
	}

}
