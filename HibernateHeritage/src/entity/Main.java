package entity;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hibern.HibernateUtil;

public class Main {

	public static void main(String[] args){
		
		CashPayment CsP = new CashPayment("cash","euros");
		CardPayment CdP = new CardPayment("carte","visa");
		Payment p = new Payment("base");
		
		createPayment(p);
		createPayment(CsP);
		createPayment(CdP);
	}

	public static void createPayment(Payment p){
		Transaction tx = null;
		Session session = HibernateUtil.currentSession();
		try{
			tx=session.beginTransaction();
			
			session.save(p);
			
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}
	}
}
