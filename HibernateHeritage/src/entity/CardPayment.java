package entity;

public class CardPayment extends Payment{
	
	private String type;

	public CardPayment(){
		
	}
	
	public CardPayment(String description, String type) {
		super(description);
		this.type = type;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "CardPayment [type=" + type + "]";
	}
	
}
