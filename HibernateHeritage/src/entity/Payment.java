package entity;

public class Payment {
	private int P_id;
	private String description;
	
	public Payment(String description) {
		super();
		this.description = description;
	}
	
	public Payment(){
		
	}

	public int getP_id() {
		return P_id;
	}

	public void setP_id(int p_id) {
		P_id = p_id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public String toString() {
		return "Payment [P_id=" + P_id + ", description=" + description + "]";
	}
	
	
	
}
