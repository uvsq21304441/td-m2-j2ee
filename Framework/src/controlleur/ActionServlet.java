package controlleur;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import actions.Action;
import utils.UsineANew;

/**
 * Servlet implementation class ActionServlet
 */
public class ActionServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//recup le pass de la jsp
		String provenance = request.getServletPath();
		UsineANew fact=null;
		try {
			fact = new UsineANew();
		} catch (ParserConfigurationException | SAXException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Action action = fact.getAction(provenance);
		String suite = action.perform(request, response);
		getServletContext()
			.getRequestDispatcher(suite)
			.forward(request, response);
	}

}
