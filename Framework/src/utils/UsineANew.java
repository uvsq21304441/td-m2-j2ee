package utils;

import java.io.IOException;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import actions.Action;

public class UsineANew {
	HashMap<String, String> actions = new HashMap<String,String>();
	
	public UsineANew() throws ParserConfigurationException, SAXException, IOException{
		ParseurXml parse = new ParseurXml();
		actions = parse.getMaMap();
	}
	
	public Action getAction(String provenance){
		Action act = null; 
		String classeAction = actions.get(provenance);
		
		try{
			Class classe  = Class.forName(classeAction);
			act = (Action) classe.newInstance();
		}catch(Exception e)
		{
			System.out.println("Do not exist");
		}
		return act;
	}
}
