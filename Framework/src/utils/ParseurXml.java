package utils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class ParseurXml {
	
	HashMap<String,String> actions = new HashMap<String,String>();
	String url =null;
	String action = null;
	
	public ParseurXml() throws ParserConfigurationException, SAXException, IOException{
		String fichier = "C:\\workspaceJEE\\Framework\\WebContent\\WEB-INF\\Config.xml";
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document document = builder.parse(fichier);
		
		Element racine = document.getDocumentElement();
		NodeList bases = racine.getElementsByTagName("mapping");
		
		for(int i = 0; i<bases.getLength();i++)
		{
			Node base = (Node) bases.item(i);
			NodeList elements = base.getChildNodes();
			for(int j=0;j<elements.getLength();j++){
				Node enfant = (Node) elements.item(j);
				if(enfant.getNodeName().equals("url"))
					url = enfant.getTextContent();
				if(enfant.getNodeName().equals("action"))
					action=enfant.getTextContent();
			}
			actions.put(url, action);
		}
	}

	public HashMap<String, String> getMaMap() {
		
		return actions;
	}
	
	public static void main(String[] args){
		try{
			ParseurXml s = new ParseurXml();
		}catch(Exception e){}
	}
}
