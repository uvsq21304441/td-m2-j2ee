package actions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class ActionQuitter implements Action {

	@Override
	public String perform(HttpServletRequest req, HttpServletResponse res) {
		String suite = null;
		System.out.println("GoodBye !");
		suite = "/GoodBye.jsp";
		return suite;
	}

}
