
package actions;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;


public class ActionLogin implements Action {

	@Override
	public String perform(HttpServletRequest req, HttpServletResponse res) {
		String log = req.getParameter("nom");
		String suite = null;
		if(log.equals("sesame"))
		{
			suite = "/Page1.jsp";
		}else
		{
			suite = "/Erreur.jsp";
		}
		return suite;
	}

}
