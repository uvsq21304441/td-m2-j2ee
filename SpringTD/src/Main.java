import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Main {
	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("beans.xml");
		
		HelloWorld helloWorld = (HelloWorld) context.getBean("helloWorld");
		helloWorld.display();
		
		ServiceLogin sl = (ServiceLogin) context.getBean("verif");
		System.out.println(sl.verif("titi","toto"));
		System.out.println(sl.verif("titi","totot"));
	}
}
