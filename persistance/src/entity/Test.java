package entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;

import hibern.HibernateUtil;

public class Test {
	public static void main(String[] args){

		Origine o1 = new Origine("France","Europe");
		Origine o2 = new Origine("Canada","Amerique");
		
		Cursus c1 = new Cursus("Maths");
		createCursus(c1);
		Cursus c2 = new Cursus("Anglais");
		createCursus(c2);
		Cursus c3 = new Cursus("Histoire");
		createCursus(c3);
		
		Set<Cursus> s1 = new HashSet<Cursus>();
		s1.add(c1);
		s1.add(c2);
		
		Set<Cursus> s2 = new HashSet<Cursus>();
		s2.add(c2);
		s2.add(c3);
		
		Set<Cursus> s3 = new HashSet<Cursus>();
		s3.add(c1);
		s3.add(c2);
		s3.add(c3);
		
		Info i = new Info("addr1","num1");
		createInfo(i);
		
		Info i2 = new Info("addr2","num2");
		createInfo(i2);
		
		UnePersonne p = new UnePersonne("p1","Mr1",i,o1,s1);
		createUnePersonnes(p);
		
		UnePersonne p2 = new UnePersonne("p2","Mr2",i,o2,s2);
		createUnePersonnes(p2);
		
		UnePersonne p3 = new UnePersonne("p3","Mr3",i2,o2,s3);
		createUnePersonnes(p3);
		
		/*listUnePersonne();*/
		
	}
	
	public static void createUnePersonnes(UnePersonne p){
		Transaction tx = null;
		Session session = HibernateUtil.currentSession();
		try{
			tx=session.beginTransaction();
			
			session.save(p);
			
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}
	}

	public static void createCursus(Cursus c){
		Transaction tx = null;
		Session session = HibernateUtil.currentSession();
		try{
			tx=session.beginTransaction();
			
			session.save(c);
			
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}
	}
	
	public static void createInfo(Info i){
		Transaction tx = null;
		Session session = HibernateUtil.currentSession();
		try{
			tx=session.beginTransaction();
			
			session.save(i);
			
			tx.commit();
		}catch(HibernateException e){
			tx.rollback();
		}
	}
	
	private static List<UnePersonne> listUnePersonne(){
		List<UnePersonne> personnes = null;
		Transaction tx = null;
		Session session = HibernateUtil.currentSession();
		
		try{
			tx = session.beginTransaction();
			personnes = session.createQuery("select x from UnePersonne as x").list();
			//for(UnePersonne p: personnes)
				//System.out.println("nom:"+p.getNom()+""+p.getPrenom());
			tx.commit();
		}catch(HibernateException e){
			e.printStackTrace();
			if(tx !=null && tx.isActive())
				tx.rollback();
		}
		
		for(UnePersonne p: personnes){
			System.out.println("\n"+p.toString());
		}
		return personnes;
	}
}
