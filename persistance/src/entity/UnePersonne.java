package entity;

import java.util.Set;

public class UnePersonne {	//Plain Old Java Object = bean
	private Long P_id; //Index
	private String nom;
	private String prenom;
	private Info info;
	private Origine origine;
	//Liste sans doublon
	private Set<Cursus> cursus;

	public UnePersonne(String nom, String prenom, Info info, Origine origine, Set<Cursus> cursus) {
		super();
		this.nom = nom;
		this.prenom = prenom;
		this.info = info;
		this.origine = origine;
		this.cursus = cursus;
	}

	public UnePersonne(){
		
	}
	
	public Origine getOrigine() {
		return origine;
	}

	public void setOrigine(Origine origine) {
		this.origine = origine;
	}

	public Long getP_id() {
		return P_id;
	}
	public void setP_id(Long p_id) {
		P_id = p_id;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}
	
	public Set<Cursus> getCursus() {
		return cursus;
	}

	public void setCursus(Set<Cursus> cursus) {
		this.cursus = cursus;
	}

	@Override
	public String toString() {
		return "UnePersonne [P_id=" + P_id + ", nom=" + nom + ", prenom=" + prenom + ", info=" + info + ", origine="
				+ origine + ", cursus=" + cursus + "]";
	}
}
