package entity;

public class Origine {
	private String pays;
	private String continent;
	
	public Origine(){
		
	}
	
	public Origine(String pays, String continent) {
		super();
		this.pays = pays;
		this.continent = continent;
	}
	public String getPays() {
		return pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}
	public String getContinent() {
		return continent;
	}
	public void setContinent(String continent) {
		this.continent = continent;
	}

	@Override
	public String toString() {
		return "Origine [pays=" + pays + ", continent=" + continent + "]";
	}
	
	
}
