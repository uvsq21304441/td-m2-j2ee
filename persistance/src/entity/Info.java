package entity;

public class Info {
	private long I_id;
	private String addr;
	private String tel;

	public Info(String addr, String tel) {
		super();
		this.addr = addr;
		this.tel = tel;
	}
	
	public Info(){
		
	}
	
	@Override
	public String toString() {
		return "info [I_id=" + I_id + ", addr=" + addr + ", tel=" + tel + "]";
	}

	public long getI_id() {
		return I_id;
	}
	public void setI_id(long i_id) {
		I_id = i_id;
	}
	public String getAddr() {
		return addr;
	}
	public void setAddr(String addr) {
		this.addr = addr;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
}
