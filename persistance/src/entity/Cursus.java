package entity;

public class Cursus {
	private long C_id;
	private String ue;
	
	public Cursus(String ue) {
		super();
		this.ue = ue;
	}
	
	public Cursus(){
		
	}

	public long getC_id() {
		return C_id;
	}

	public void setC_id(long c_id) {
		C_id = c_id;
	}

	public String getUe() {
		return ue;
	}

	public void setUe(String ue) {
		this.ue = ue;
	}

	@Override
	public String toString() {
		return "Cursus [C_id=" + C_id + ", ue=" + ue + "]";
	}
	
	
}
