package action;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.HelloWorldForm;

public class HelloWorldAction extends Action {
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		HelloWorldForm hwform = (HelloWorldForm) form;
		System.out.println(hwform.getLogin() + " " + hwform.getPass());
		
		if(hwform.isValid()) {
			
			Map<String, String> articles = new HashMap<String, String>();
			HttpSession session = request.getSession();
			session.setAttribute("articles", articles);
			
			return mapping.findForward("suite");
		}
		return mapping.findForward("erreur");
	}
}
