package action;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.ArticleForm;

public class AjoutAction extends Action {
	
	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		
		ArticleForm aform = (ArticleForm) form;
		HttpSession session = request.getSession();
		
		System.out.println(aform.getArticle() + " " + aform.getQuantite());
		
		Map<String, String> articles = (Map<String, String>) session.getAttribute("articles");
		articles.put(aform.getArticle(), aform.getQuantite());
		
		return mapping.findForward("suite");
	}

}
