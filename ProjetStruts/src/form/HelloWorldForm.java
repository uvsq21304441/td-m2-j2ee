package form;

import org.apache.struts.action.ActionForm;

public class HelloWorldForm extends ActionForm {

	private String login;
	private String pass;
	
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public boolean isValid() {
		return !pass.equals("");
	}
	
}
