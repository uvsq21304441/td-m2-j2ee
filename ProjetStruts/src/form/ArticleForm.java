package form;

import org.apache.struts.action.ActionForm;

public class ArticleForm extends ActionForm {
	
	private String article;
	private String quantite;

	public String getArticle() {
		return article;
	}

	public void setArticle(String article) {
		this.article = article;
	}

	public String getQuantite() {
		return quantite;
	}

	public void setQuantite(String quantite) {
		this.quantite = quantite;
	}
}
