<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="java.util.Map,java.util.Set" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1>C'est la suite de Hello, Super !</h1>
	
	<%
		out.println("<ul>");
		Map<String, String> articles = (Map<String, String>) session.getAttribute("articles");
		
		Set<String> keys = articles.keySet();
		for(String key : keys) {
			out.println("<li>" + key + " " + articles.get(key) + "</li>");
		}
		out.println("</ul>");
	%>
</body>
</html>