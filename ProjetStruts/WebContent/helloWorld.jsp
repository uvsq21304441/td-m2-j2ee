<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="form.HelloWorldForm" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Hello</title>
</head>
<body>
	<h1>
		<%
			HelloWorldForm hwform = (HelloWorldForm) session.getAttribute("login");
			out.print("Coucou " + hwform.getLogin());
		%>
	</h1>
	
	<form action="ajout.do" method="POST">
		Article : <input type="text" name="article"><br/>
		Quantite : <input type="text" name="quantite">
		Ajouter : <input type="submit" value="Ajouter">
	</form>
	
	<form action="toto.do" method="POST">
		<input type="submit" value="Afficher le panier">
	</form>
</body>
</html>