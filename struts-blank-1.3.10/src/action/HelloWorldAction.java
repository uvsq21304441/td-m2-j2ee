package action;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import form.IndexForm;

public class HelloWorldAction extends Action {

	@Override
	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		// TODO Auto-generated method stub
		IndexForm indexform = (IndexForm) form;
		
		String suite = null;
		if(indexform.isOk()){
			
			HttpSession session = request.getSession();
			List<String> achat = new ArrayList<String>();
			session.setAttribute("achats",achat);
			
			suite = "suiteOk";
		}else{
			suite = "suiteNotOk";
		}
		return mapping.findForward(suite);
	}
}
