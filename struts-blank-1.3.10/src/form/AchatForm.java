package form;

import org.apache.struts.action.ActionForm;

public class AchatForm extends ActionForm {

	private static final long serialVersionUID = 1L;
	private String obj;
	private String qt;
	
	
	public String getObj() {
		return obj;
	}
	public void setObj(String obj) {
		this.obj = obj;
	}
	public String getQt() {
		return qt;
	}
	public void setQt(String qt) {
		this.qt = qt;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	
	
}
