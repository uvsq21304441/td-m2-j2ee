<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page import="java.util.ArrayList, beans.Cv" %>
<jsp:useBean id="cv" class="beans.Cv" scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<%@ include file="entete.jsp" %>
	<c:forEach var="cv" items="${cvs}">
		<c:out value="${cv}"/><br/>
	</c:forEach>
	
	<form action="Accueil.jsp" method="post">
		<p>
    		<button type="submit">Ajouter nouveau CV</button>
    	</p>
	</form>
	
<%@ include file="footer.jsp" %>
</body>
</html>