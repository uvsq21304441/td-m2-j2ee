<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page errorPage="erreur.jsp" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:useBean id="cv" class="beans.Cv" scope="request"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

<%@ include file="entete.jsp" %>
	
	<c:out value="${cv.prenom}"></c:out> suit les cours :
	<br/>
	<c:forEach var="cours" items="${paramValues.cours }">
		<c:out value="${cours }"/><br/>
	</c:forEach>
	<c:if test="${!empty param.voeux }">
		Et veut suivre: <br/>
		<c:forTokens delims="," items="${param.voeux}" var="voeux">
			<c:out value="${voeux}"></c:out><br/>
		</c:forTokens>
	</c:if>
	<form action="Accueil.jsp" method="post">
		<p>
    		<button type="submit">Ajouter nouveau CV</button>
    	</p>
	</form>
	<form action="affiche" method="post">
  		<p>
    		<button type="submit">Liste CV</button>
		</p>
	</form>
<%@ include file="footer.jsp" %>
</body>
</html>