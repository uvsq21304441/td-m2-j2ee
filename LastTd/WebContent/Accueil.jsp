<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page errorPage="/WEB-INF/erreur.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet" type="text/css" href="CSSIndex.css">
</head>
<body>
	
<%@ include file="/WEB-INF/entete.jsp" %>
	<form action="SignUp" method="post">
		<fieldset>
			<p>
		    	<label for="prenom">Prenom</label><br>
		    	<input type="text" id="prenom" name="prenom" required>
		  	</p>
		  	<p>
		    	<label for="nom">Nom</label><br>
		    	<input type="text" id="nom" name="nom" required>
		  	</p>
		  	<p>
		    	<label for="age">Age</label><br>
		    	<input type="text" id="age" name="age" required>
		  	</p>
		  	<p>
		    	<label for="level">Niveau d'�tude</label><br>
		    	<select name="level" id="level">
		      		<option value="bac">Bac</option>
		      		<option value="bac +3">Bac+3</option>
		      		<option value="bac +5">Bac+5</option>
		   		 </select>
	  		</p>
	  		<p>
		    	<label for="cours">Cours</label><br>
		    	<select name="cours" id="cours" multiple="multiple">
		      		<option value="J2EE">J2EE</option>
		      		<option value="C">C</option>
		      		<option value="Algo">Algo</option>
		      		<option value="Graphe">Graphe</option>
		   		 </select>
	  		</p>
	  		<p>
	  		<label for="voeux"> Cours que vous voulez suivre</label><br/>
	  			<textarea id="voeux" name="voeux" rows="5" cols="50"></textarea>
	  		</p>
	  		<p>
	    		<button type="submit">Ajouter</button>
	    		<button type="reset">Reset</button>
			</p>
		</fieldset>
	</form>
<%@ include file="/WEB-INF/footer.jsp" %>
</body>
</html>