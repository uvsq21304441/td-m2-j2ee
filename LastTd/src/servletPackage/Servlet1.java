package servletPackage;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Cv;

/**
 * Servlet implementation class Servlet1
 */
public class Servlet1 extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
    private ArrayList<Cv> cvs = new ArrayList<Cv>();
	
    public Servlet1() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("DoGet");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		
		Cv cv = new Cv(request.getParameter("nom"),request.getParameter("prenom"),Integer.parseInt(request.getParameter("age")),request.getParameter("level"));
		this.cvs.add(cv);
		session.setAttribute("cvs", cvs);
		request.setAttribute("cv", cv);
		getServletContext().getRequestDispatcher("/WEB-INF/ajoutOk.jsp").forward(request, response);
	}

}
