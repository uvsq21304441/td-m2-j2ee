package beans;

public class Cv {
	private String nom;
	private String prenom;
	private int age;
	private String LvlEtude;
	
	public Cv(){
		
	}

	public Cv(String nom, String prenom, int age, String LvlEtude) {
		this.nom = nom;
		this.prenom = prenom;
		this.age = age;
		this.LvlEtude = LvlEtude;
	}

	public String getLvlEtude() {
		return LvlEtude;
	}

	public void setLvlEtude(String lvlEtude) {
		LvlEtude = lvlEtude;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Cv de " + nom + " " + prenom + ", age=" + age + ", Niveau d'�tude=" + LvlEtude;
	}
}
