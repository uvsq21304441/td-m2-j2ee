package entity;

public class Personne {
	private int id;
	private String nom;
	private int age;
	
	public Personne(){
		
	}
	
	public Personne(String nom, int age) {
		super();
		this.nom = nom;
		this.age = age;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getnom() {
		return nom;
	}

	public void setnom(String nom) {
		this.nom = nom;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public String toString() {
		return "Personne [id=" + id + ", nom=" + nom + ", age=" + age + "]";
	}
	
	
	
}
