package entity;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.DAOPersonne;
import dao.IDAOPersonne;
import spy.Greffon;

public class Main {

	public static void main(String[] args){
		ApplicationContext context = new ClassPathXmlApplicationContext("MyBeans.xml");
		IDAOPersonne daoPersonne = (DAOPersonne)context.getBean("DAOPersonne");

		Personne personne = new Personne ("Jojo la Patate",30);
		daoPersonne.savePersonne(personne);
		Personne pers = daoPersonne .findById(1);
		System.out.println(pers);
		
		/*ApplicationContext context = new ClassPathXmlApplicationContext("MyBeans.xml");
		
		Greffon gref = (Greffon) context.getBean("decore");
	
		FournisseurDeQuestion SQ = (FournisseurDeQuestion) context.getBean("QservTiti"); 
		SQ.poseQuestion();
		
		ServiceLogin sl = (ServiceLogin) context.getBean("ServLog");
		sl.setLogin("yannick");
		sl.MaJPass("sesame");
		System.out.println(sl.toString());*/
	}
}