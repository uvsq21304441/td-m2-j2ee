package entity;

public class FournisseurDeQuestion {
	private Question question;
	
	public void poseQuestion(){
		System.out.println(question.popQuestion());
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}
}
