package entity;

import dao.IDAOPersonne;

public class ServicePersonne implements IServicePersonne{
	private IDAOPersonne idaoPersonne;
	
	public void savePersonne(Personne personne){
		idaoPersonne.savePersonne(personne);
		}
	public void deletePersonne(Personne personne){
		idaoPersonne.deletePersonne(personne);
		}
	public Personne findById(int id){
		 return idaoPersonne.findById(id);
	}
	public IDAOPersonne getIdaoPersonne() {
		return idaoPersonne;
	}
	public void setIdaoPersonne(IDAOPersonne idaoPersonne) {
		this.idaoPersonne = idaoPersonne;
	}
	
	
}
