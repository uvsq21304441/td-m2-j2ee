package entity;

public interface IServicePersonne {
	void savePersonne(Personne personne);
	void deletePersonne(Personne personne);
	Personne findById(int id);
}
