package entity;

public class ServiceLogin {
	private String login;
	private String mdp;
	
	public ServiceLogin(){
		
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMdp() {
		return mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	
	public void MaJPass(String Mdp){
		this.setMdp(Mdp);
	}
	
	public boolean verif(String log, String mp){
		if(log.equals("titi") && mp.equals("toto")){
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		return "ServiceLogin [login=" + login + ", mdp=" + mdp + "]";
	}
	
	
}
