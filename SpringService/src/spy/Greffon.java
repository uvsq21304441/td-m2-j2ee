package spy;

import org.aspectj.lang.JoinPoint;

public class Greffon {

	public void ecrireAvant(JoinPoint jp){
		System.out.println("Avant");
	}
	
	public void ecrireApres(){
		System.out.println("Apres");
	}
}
