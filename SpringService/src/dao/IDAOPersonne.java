package dao;

import entity.Personne;

public interface IDAOPersonne extends InterfaceDAO<Personne> {

	public void savePersonne(Personne personne);
	public void deletePersonne(Personne personne);
	public Personne findById(int id);
}
