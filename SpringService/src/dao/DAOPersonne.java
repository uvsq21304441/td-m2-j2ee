package dao;

import java.util.List;

import org.springframework.orm.hibernate3.support.HibernateDaoSupport;

import entity.Personne;

public class DAOPersonne extends HibernateDaoSupport implements IDAOPersonne{

	@Override
	public void savePersonne(Personne personne) {
		getHibernateTemplate().save(personne);
	}

	@Override
	public void deletePersonne(Personne personne) {
		getHibernateTemplate().delete(personne);
		
	}

	@Override
	public Personne findById(int id) {
		return (Personne)getHibernateTemplate().get(Personne.class,id);
	}

	public void persist(Personne Personne) {
		getHibernateTemplate().persist(Personne);
	}

	public void delete(Personne Personne) {
		getHibernateTemplate().delete(Personne);
	}

	public Personne findById(Integer id) {
		return (Personne) getHibernateTemplate().get(Personne.class, id);
	}

	public List find(String query) {
		return getHibernateTemplate().find(query);
	}

	public List find(String query, Object[] params) {
		return getHibernateTemplate().find(query, params);
	}
}
