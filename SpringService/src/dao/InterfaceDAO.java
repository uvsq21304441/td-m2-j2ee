package dao;

import entity.Personne;

public interface InterfaceDAO<T> {

	public void savePersonne(T obj);
	public void deletePersonne(T obj);
	public Personne findById(int id);
}
